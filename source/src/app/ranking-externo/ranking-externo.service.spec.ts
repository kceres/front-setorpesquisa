import { TestBed } from '@angular/core/testing';

import { RankingExternoService } from './ranking-externo.service';

describe('RankingExternoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RankingExternoService = TestBed.get(RankingExternoService);
    expect(service).toBeTruthy();
  });
});
