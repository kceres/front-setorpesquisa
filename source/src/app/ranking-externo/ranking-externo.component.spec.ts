import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RankingExternoComponent } from './ranking-externo.component';

describe('RankingExternoComponent', () => {
  let component: RankingExternoComponent;
  let fixture: ComponentFixture<RankingExternoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RankingExternoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RankingExternoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
