// Import libs
import { Component } from '@angular/core';

// Import components, modules and service
import { AppService } from './app.service';
import { Router, NavigationEnd } from '@angular/router';
import { longStackSupport } from 'q';
import { CookieService } from 'ngx-cookie-service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';
  componentRef: any;

  constructor(
    public appService: AppService,
    private _router: Router,
    private cookieService: CookieService
  ) { }

  ngOnInit() {
    this._router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        window.scrollTo(0, 0);
        if (val.url == '/login') {
          localStorage.removeItem('usuarioSetorPesquisa');
          this.cookieService.delete("setorPesquisaLogin");
          this.appService.removeCookie("setorPesquisaLogin");
          this.appService.isLogged = false;
        } else {
          let statusLogin = this.appService.getCookie("setorPesquisaLogin");

          let user = localStorage.getItem('usuarioSetorPesquisa');
          
          //if (statusLogin == undefined) {
            if (user == undefined) {
            this.appService.removeCookie("setorPesquisaLogin");
            this.cookieService.delete("setorPesquisaLogin");
            this.appService.isLogged = false;
            this._router.navigate(["/login"]);
            return;
          }
        //}

          this.appService.isLogged = true;
        }
      }
    });
  }
}
