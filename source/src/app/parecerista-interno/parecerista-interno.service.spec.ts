import { TestBed } from '@angular/core/testing';

import { PareceristaInternoService } from './parecerista-interno.service';

describe('PareceristaInternoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PareceristaInternoService = TestBed.get(PareceristaInternoService);
    expect(service).toBeTruthy();
  });
});
