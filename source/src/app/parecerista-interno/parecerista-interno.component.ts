import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PareceristaInternoService } from './parecerista-interno.service';
import { AppService } from '../app.service';

@Component({
  selector: 'app-parecerista-interno',
  templateUrl: './parecerista-interno.component.html',
  styleUrls: ['./parecerista-interno.component.scss']
})
export class PareceristaInternoComponent implements OnInit {

  formCadastraParecerista: FormGroup;
  verifyLoader: boolean;
  pareceristaInterno: any;
  numFuncional = new FormControl('');
  checkStatus = new FormControl('');
  verficaCadastro: boolean = true;
  verificaNome: boolean = true;
  verificaCurso: boolean = true;
  lstPareceristaInterno: any;
  filtrado: any;
  p: number = 1;

  constructor(
    public _pareceristaInternoService: PareceristaInternoService,
    public router: Router,
    public route: ActivatedRoute,
    public appService: AppService
  ) {
    this.formCadastraParecerista = new FormGroup({
      numFuncional: new FormControl('')
    });
  }

  ngOnInit() {
    this.ListarPareceristaInterno();
  }

  ListarPareceristaInterno() {
    this._pareceristaInternoService.ListarPareceristaInterno()
      .subscribe((data: any) => {
        this.lstPareceristaInterno = data;
        this.filtrado = data;
        console.log(data);

      }, err => {
        this.appService.showToastrWarning("Não foi possível localizar nenhum parecerista!");
      });
  }

  buscarOrientador() {
    let func = this.numFuncional.value;

    if (func == '' || func == undefined || func.lenght <= 6) {
      this.appService.showToastrWarning(`Preencha os campos corretamente.`);
      return;
    }

    this._pareceristaInternoService.retornaOrientador(this.numFuncional.value)
      .subscribe((data: any) => {

        this.pareceristaInterno = data;

        if (this.pareceristaInterno.Nome == undefined) {
          this.appService.showToastrWarning("Orientador não encontrado!");
          return;
        }
        if (this.pareceristaInterno.PareceristaInterno) {
          this.appService.showToastrWarning("Orientador é um Parecerista!");
          return;
        }

        this.pareceristaInterno.Titulacao = this.verificarTitulacao(this.pareceristaInterno.Titulacao);

        if (this.pareceristaInterno.Curso != undefined)
          this.verificaCurso = false;

        this.verficaCadastro = false;
        this.verificaNome = false;

      }, err => {
        this.appService.showToastrWarning("Orientador não encontrado!");
      });
  }

  cadastrarParecerista() {
    let body = {
      NumeroFuncional: this.numFuncional.value,
      Parecerista: true,
      StatusParecerista: true
    }

    this._pareceristaInternoService.DefineParecerista(body)
      .subscribe(
        (res: any) => {


          this.appService.showToastrSuccess(`Parecerista cadastrado com sucesso!`);

          this.numFuncional.setValue('');
          this.verficaCadastro = true;
          this.verificaNome = true;
          this.pareceristaInterno = null;
          this.ListarPareceristaInterno();
        },
        err => {
          this.verifyLoader = !this.verifyLoader;
          this.appService.showToastrError(`Ocorreu algum erro inesperado.`);
        }
      );
  }

  alteraStatus(numFunc: any) {
    numFunc.StatusParecerista = !numFunc.StatusParecerista;

    let body = {
      NumeroFuncional: numFunc.NumeroFuncional,
      StatusParecerista: numFunc.StatusParecerista
    }

    this._pareceristaInternoService.AlteraStatusParecerista(body)
      .subscribe(
        (res: any) => {

          if (!numFunc.StatusParecerista)
            this.appService.showToastrSuccess(`Parecerista interno desabilidado no sistema!`);
          else
            this.appService.showToastrSuccess(`Parecerista interno habilitado no sistema!`);
        },
        err => {
          this.verifyLoader = !this.verifyLoader;
          this.appService.showToastrError(`Não foi possível alterar o status.`);
        }
      );
  }

  verificarTitulacao(_titulacao: any) {

    switch (_titulacao) {
      case "M":
        return "Mestre"
      case "D":
        return "Doutor"
      case "A":
        return "Mestre/Doutor"
      default:
        return '--'
    }
  }


  filtrarProjetos(event: any) {
    let srtFiltro = this.appService.normalizarString(event.target.value.toLowerCase());

    this.filtrado = this.lstPareceristaInterno.filter((el) => {
      return this.appService.normalizarString(el.NumeroFuncional).toLowerCase().indexOf(srtFiltro) > -1 ||
        this.appService.normalizarString(el.Nome).toLowerCase().indexOf(srtFiltro) > -1 ||
        this.appService.normalizarString(el.Email).toLowerCase().indexOf(srtFiltro) > -1 ||
        this.appService.normalizarString(this.verificarTitulacao(el.Titulacao).toLowerCase()).indexOf(srtFiltro) > -1 ||
        this.appService.normalizarString(el.Lates).toLowerCase().indexOf(srtFiltro) > -1;


    });
  }
}
