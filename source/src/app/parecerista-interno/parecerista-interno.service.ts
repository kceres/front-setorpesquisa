import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { map } from "rxjs/operators";
import { baseUrl } from '../shared/shared.variables';
import { AppService } from '../app.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PareceristaInternoService {

  _serviceAuthentication: string = `${baseUrl}parecerista-internoSetorPesquisa`;

  constructor(
    public _http: HttpClient,
    public appService: AppService
  ) { }

  retornaOrientador(_numeroFuncional) {
    return this._http.get(`${baseUrl}Professor/GetProfessorByNumeroFuncional?numeroFuncional=${_numeroFuncional}`,this.appService.headerOptions);
  }

  DefineParecerista(obj): any {
    return this._http.post(`${baseUrl}PareceristaInterno/DefineParecerista`, this.appService.tratarObjeto(obj), this.appService.headerOptions);
  }

  ListarPareceristaInterno(){
    return this._http.get(`${baseUrl}PareceristaInterno/Listar`,this.appService.headerOptions);
  }

  AlteraStatusParecerista(obj): any {
    return this._http.post(`${baseUrl}PareceristaInterno/DefineStatusParecerista`, this.appService.tratarObjeto(obj), this.appService.headerOptions);
  }
}
