import { TestBed } from '@angular/core/testing';

import { GrupoPesquisaService } from './grupo-pesquisa.service';

describe('GrupoPesquisaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GrupoPesquisaService = TestBed.get(GrupoPesquisaService);
    expect(service).toBeTruthy();
  });
});
