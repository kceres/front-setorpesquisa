import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { map } from "rxjs/operators";
import { baseUrl } from '../shared/shared.variables';
import { AppService } from '../app.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GrupoPesquisaService {

  constructor(
    public _http: HttpClient,
    public appService: AppService
  ) { }

  listarGrupoPesquisa() {
    return this._http.get(`${baseUrl}GrupoPesquisa/Listar`,this.appService.headerOptions);
  }

  listarAreaAtuacao(idGrupo: any){
    return this._http.get(`${baseUrl}AreaAtuacao/ListarGrupo?idGrupo=${idGrupo}`,this.appService.headerOptions);
  }

  incluirAreaAtuacao(areaAtuacao: any){
    return this._http.post(`${baseUrl}AreaAtuacao/Incluir`,this.appService.tratarObjeto(areaAtuacao), this.appService.headerOptions);
  }

  alterarAreaAtuacao(areaAtuacao: any){
    return this._http.post(`${baseUrl}AreaAtuacao/Alterar`, this.appService.tratarObjeto(areaAtuacao), this.appService.headerOptions);
  }

  alterarStatusAreaAtuacao(alterarStatus: any){
    return this._http.post(`${baseUrl}AreaAtuacao/AlterarStatus`,this.appService.tratarObjeto(alterarStatus), this.appService.headerOptions);
  }
}