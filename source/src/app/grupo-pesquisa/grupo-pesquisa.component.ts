import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, FormControlName } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import { GrupoPesquisaService } from './grupo-pesquisa.service';
import { AppService } from '../app.service';

@Component({
  selector: 'app-grupo-pesquisa',
  templateUrl: './grupo-pesquisa.component.html',
  styleUrls: ['./grupo-pesquisa.component.scss']
})
export class GrupoPesquisaComponent implements OnInit {
  gruposPesquisa: any;
  areas: any;
  grupoId: any;
  formAreaAtuacao: FormGroup;
  idAreaAtuacao = new FormControl('');
  areaAtuacao = new FormControl('');
  checkStatus = new FormControl('');

  constructor(
    public _grupoPesquisaService: GrupoPesquisaService,
    public route: ActivatedRoute,
    public router: Router,
    public appService: AppService
  ) {
    this.formAreaAtuacao = new FormGroup({
      idAreaAtuacao: new FormControl(''),
      areaAtuacao: new FormControl('')
    });
  }

  ngOnInit() {
    this.listarGrupoPesquisa();

  }

  listarGrupoPesquisa() {
    this._grupoPesquisaService.listarGrupoPesquisa().subscribe((data: any) => {
      this.gruposPesquisa = data;

      this.listarAreaAtuacao(this.gruposPesquisa[0].Id);

    }, err => {
      this.appService.showToastrWarning("Não foi possível localizar nenhum grupo de pesquisa!");
    });
  }

  listarAreaAtuacao(idGrupo: any) {

    this._grupoPesquisaService.listarAreaAtuacao(idGrupo).subscribe((data: any) => {
      this.areas = data;
      this.grupoId = this.areas[0].IdGrupoPesquisa;

    }, err => {
      this.appService.showToastrWarning("Não foi possível localizar nenhuma área de atuação!");
    });
  }

  incluirAreaAtuacao() {
    this.formAreaAtuacao.setValue({ idAreaAtuacao: '', areaAtuacao: '' });
  }

  editarGrupo(id: number, descricao: string) {
    this.formAreaAtuacao.setValue({ idAreaAtuacao: id, areaAtuacao: descricao });
  }

  alteraStatus(_obj: any) {
    _obj.Status = !_obj.Status;

    let body = {
      Id: _obj.Id,
      Status: _obj.Status
    }

    this._grupoPesquisaService.alterarStatusAreaAtuacao(body)
      .subscribe(
        (res: any) => {
          if (!_obj.Status)
            this.appService.showToastrSuccess(`Área atuação desabilitada no sistema!`);
          else
            this.appService.showToastrSuccess(`Área atuação habilitado no sistema!`);

        },
        err => {
          this.appService.showToastrError(`Não foi possível alterar o status.`);
        }
      );
  }

  gestaoAreaAtuacao() {
    let body: any;

    if (this.formAreaAtuacao.value.idAreaAtuacao == '') {

      body = {
        IdGrupo: this.grupoId,
        AreaAtuacao: this.formAreaAtuacao.value.areaAtuacao
      };
      this._grupoPesquisaService.incluirAreaAtuacao(body).subscribe((data: any) => {
        this.appService.showToastrSuccess("Área de atuação cadastrada com sucesso!");
        this.listarGrupoPesquisa();
      }, err => {
        this.appService.showToastrError("não foi possível cadastrar a área de atuação!");
      });
    } else {

      body = {
        Id: this.formAreaAtuacao.value.idAreaAtuacao,
        AreaAtuacao: this.formAreaAtuacao.value.areaAtuacao
      };

      this._grupoPesquisaService.alterarAreaAtuacao(body).subscribe((data: any) => {
        this.appService.showToastrSuccess("Área de atuação alterada com sucesso!");
        this.listarGrupoPesquisa();
      }, err => {
        this.appService.showToastrError("Não foi possível alterar a área de atuação!");
      });
    }
  }
}
