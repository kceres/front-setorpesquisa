import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import { LoginService } from './login.service';
import { AppService } from '../app.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  formLogin: FormGroup;
  verifyLoader: boolean;

  constructor(
    public _loginService: LoginService,
    public router: Router,
    public appService: AppService
  ) {
    this.formLogin = new FormGroup({
      identificacao: new FormControl(),
      senha: new FormControl()
    });
  }

  ngOnInit() { }

  doLogin() {
    // this.router.navigate(["/home"]);
    // return;

    let body = this.formLogin.value;
    this.verifyLoader = true;
    this._loginService.authetication(body)
      .subscribe(
        (res: any) => {
          this.verifyLoader = !this.verifyLoader;
          if(res.Valido) {
            localStorage.setItem('usuarioSetorPesquisa', JSON.stringify(res));
            this.appService.createCookie("setorPesquisaLogin", true);
            this.appService.isLogged = true;
            this.router.navigate(["/home"]);
          } else {
            localStorage.removeItem('usuarioSetorPesquisa');
            this.appService.removeCookie("setorPesquisaLogin");
            this.appService.isLogged = false;
            this.appService.showToastrError(res.Mensagem);
          }

        },
        err => {
          console.log(err);
          this.verifyLoader = !this.verifyLoader;
          this.appService.showToastrError(`Ocorreu algum erro inesperado`);
        }
      );
  }

}
