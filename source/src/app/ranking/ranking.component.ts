import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import { RankingService } from './ranking.service';
import { AppService } from '../app.service';
import { baseUrl } from '../shared/shared.variables';
import { VisualizacaoProjetoService } from '../lista-projetos/visualizacao-projeto/visualizacao-projeto.service';

@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.component.html',
  styleUrls: ['./ranking.component.scss']
})
export class RankingComponent implements OnInit {
  baseUrl: string = baseUrl;
  aluno: any;
  lstProjetos: any;
  resumo: any;
  detalhe: any;
  pareceristaInterno: any;
  filtrado:any;
  p: number = 1;

  bloqueioHistorico: boolean = false;
  historicoCarregado: boolean;

  constructor(
    public _rankingService: RankingService,
    public route: ActivatedRoute,
    public router: Router,
    public appService: AppService,
    public _visualizacaoProjetoService: VisualizacaoProjetoService,
  ) {
  }

  ngOnInit() { 
    this.listarRanking();
  }

  listarRanking(){
    this._rankingService.listarProjetosRanking().subscribe((data: any) => {
      this.lstProjetos = data;
      this.filtrado = data;
      console.log(data);

    }, err => {
      this.appService.showToastrWarning("Não foi possível localizar nenhum projeto.");
    });
  }
  
  carregarResumo(resumo: any){
    this.resumo = resumo;
  }

  carregarDetalhe(_detalhe: any){
    this.detalhe = _detalhe;
    this.carregarParecerista();
  }

  retornaHistorico() {
    this._visualizacaoProjetoService.retornaHistoricoEscolar(this.detalhe.Ra)
      .subscribe((data: any) => {
        this.aluno = data;
        this.historicoCarregado = true;
        this.bloqueioHistorico = false;
      }, (error: any) => {
        this.historicoCarregado = true;
        this.bloqueioHistorico = true;
        this.appService.showToastrError("Não foi possível localizar seu histórico escolar.");
        return;
      });
  }

  carregarParecerista() {
    this._visualizacaoProjetoService.retornarPareceristaInterno(this.detalhe.PareceristaInterno)
      .subscribe((data: any) => {
        this.pareceristaInterno = data;
        this.retornaHistorico();
       
      }, (error: any) => {
        this.appService.showToastrError("Não foi possível localizar o parecerista do projeto.");
        return;
      });
  }

  filtrarProjetos(event: any){
    let srtFiltro = this.appService.normalizarString(event.target.value.toLowerCase());

    this.filtrado = this.lstProjetos.filter((el) => {
      return this.appService.normalizarString(el.Aluno).toLowerCase().indexOf(srtFiltro) > -1||
             this.appService.normalizarString(el.Email).toLowerCase().indexOf(srtFiltro) > -1||
             this.appService.normalizarString(el.EmailOrientador).toLowerCase().indexOf(srtFiltro) > -1||
             this.appService.normalizarString(el.Orientador).toLowerCase().indexOf(srtFiltro) > -1||
             this.appService.normalizarString(el.Situacao).toLowerCase().indexOf(srtFiltro) > -1||
             this.appService.normalizarString(el.Turma).toLowerCase().indexOf(srtFiltro) > -1||
             this.appService.normalizarString(el.Campus).toLowerCase().indexOf(srtFiltro) > -1||
             this.appService.normalizarString(el.Protocolo).toLowerCase().indexOf(srtFiltro) > -1||
             this.appService.normalizarString(el.Projeto).toLowerCase().indexOf(srtFiltro) > -1||
             this.appService.normalizarString(el.Curso).toLowerCase().indexOf(srtFiltro) > -1||
             this.appService.normalizarString(el.Turma).toLowerCase().indexOf(srtFiltro) > -1||
             this.appService.normalizarString(el.TipoAluno).toLowerCase().indexOf(srtFiltro) > -1;
     });
  }
}
