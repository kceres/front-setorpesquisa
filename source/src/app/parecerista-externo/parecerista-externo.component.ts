import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { PareceristaExternoService } from "./parecerista-externo.service";
import { AppService } from '../app.service';

@Component({
  selector: 'app-parecerista-externo',
  templateUrl: './parecerista-externo.component.html',
  styleUrls: ['./parecerista-externo.component.scss']
})
export class PareceristaExternoComponent implements OnInit {

  formCadastraParecerista: FormGroup;
  nomeParecerista = new FormControl('');
  emailParecerista = new FormControl('');
  verficaCadastro: boolean = true;
  lstPareceristaExterno: any;
  checkStatus = new FormControl('');

  constructor(
    public _pareceristaExternoService: PareceristaExternoService,
    public router: Router,
    public route: ActivatedRoute,
    public appService: AppService
  ) {
    this.formCadastraParecerista = new FormGroup({
      nomeParecerista: new FormControl('', [Validators.required, Validators.nullValidator, Validators.minLength(5)]),
      emailParecerista: new FormControl('', [Validators.required, Validators.email, Validators.nullValidator, Validators.minLength(5)])
    });
  }

  ngOnInit() {
    this.ListarPareceristaExterno();
  }

  ListarPareceristaExterno() {
    this._pareceristaExternoService.ListarPareceristaExterno()
      .subscribe((data: any) => {
        this.lstPareceristaExterno = data;

      }, err => {
        this.appService.showToastrWarning("Não foi possível localizar nenhum parecerista!");
      });
  }

  cadastrarPareceristaExterno() {
    let body = {
      Nome: this.formCadastraParecerista.value.nomeParecerista,
      Email: this.formCadastraParecerista.value.emailParecerista
    }

    if (!this.formCadastraParecerista.valid) {
      this.appService.showToastrWarning(`Preencha os campos corretamente.`);
      return;
    }

    this._pareceristaExternoService.ConvidaPareceristaExterno(body)
      .subscribe(
        (res: any) => {


          this.appService.showToastrSuccess(`Parecerista cadastrado com sucesso!`);
          this.verficaCadastro = true;
          this.ListarPareceristaExterno();
        },
        err => {
          this.appService.showToastrError(`Ocorreu algum erro inesperado.`);
        }
      );
  }

  alteraStatus(_obj: any) {
    _obj.StatusParecerista = !_obj.StatusParecerista;

    let body = {
      idPareceristaExterno: _obj.Id,
      Status: _obj.StatusParecerista
    }

    this._pareceristaExternoService.AlteraStatusParecerista(body)
      .subscribe(
        (res: any) => {
          if (!_obj.StatusParecerista)
            this.appService.showToastrSuccess(`Parecerista externo desabilidado no sistema!`);
          else
            this.appService.showToastrSuccess(`Parecerista externo habilitado no sistema!`);

        },
        err => {
          this.appService.showToastrError(`Não foi possível alterar o status.`);
        }
      );
  }

  reenviarConvite(_email: any) {
    let body = {
      Email: _email
    }

    this._pareceristaExternoService.ReenviarConvite(body)
      .subscribe(
        (res: any) => {
          this.appService.showToastrSuccess(`Convite reenviado com sucesso!`);
        },
        err => {
          this.appService.showToastrWarning(`Não foi possivel reenviar, parecerista externo com cadastro finalizado!`);
        }
      );
  }

  reiniciarSenha(id: any){
    let body = {
      Id: id
    }

    this._pareceristaExternoService.reiniciarSenha(body)
    .subscribe(
      (res: any) => {
        this.appService.showToastrSuccess(`A senha do(a) parecerista externo, foi reiniciada com sucesso!`);
      },
      err => {
        this.appService.showToastrWarning(`Não foi possivel reiniciadar a senha do(a) parecerista externo!`);
      }
    );
  }

  statusCadastro(_statusCadastro: any) {

    return _statusCadastro ? "Completo" : "Pendente";
  }
}