import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { PareceristaExternoComponent } from './parecerista-externo.component';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import {NgbModule, NgbAlertModule, NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [PareceristaExternoComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule,
    NgbModule,
    NgbPaginationModule,
    NgbAlertModule
  ]
})
export class PareceristaExternoModule { }
