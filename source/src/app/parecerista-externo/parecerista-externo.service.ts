import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { map } from "rxjs/operators";
import { baseUrl } from '../shared/shared.variables';
import { AppService } from '../app.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PareceristaExternoService {

  constructor(
    public _http: HttpClient,
    public appService: AppService
  ) { }

  ListarPareceristaExterno(){
    return this._http.get(`${baseUrl}PareceristaExterno/Listar`,this.appService.headerOptions);
  }

  ConvidaPareceristaExterno(obj): any {
    return this._http.post(`${baseUrl}PareceristaExterno/ConvidarParecerista`, this.appService.tratarObjeto(obj), this.appService.headerOptions);
  }

  AlteraStatusParecerista(obj): any {
    return this._http.post(`${baseUrl}PareceristaExterno/AlteraStatusParecerista`, this.appService.tratarObjeto(obj), this.appService.headerOptions);
  }

  ReenviarConvite(obj): any {
    return this._http.post(`${baseUrl}PareceristaExterno/ReenviarEmailPareceristaExterno`, this.appService.tratarObjeto(obj), this.appService.headerOptions);
  }

  reiniciarSenha(obj): any {
    return this._http.post(`${baseUrl}PareceristaExterno/ReiniciarSenha`, this.appService.tratarObjeto(obj), this.appService.headerOptions);
  }
  
}
