import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PareceristaExternoComponent } from './parecerista-externo.component';

describe('PareceristaExternoComponent', () => {
  let component: PareceristaExternoComponent;
  let fixture: ComponentFixture<PareceristaExternoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PareceristaExternoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PareceristaExternoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
