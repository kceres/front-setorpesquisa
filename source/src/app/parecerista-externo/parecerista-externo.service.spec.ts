import { TestBed } from '@angular/core/testing';

import { PareceristaExternoService } from "./parecerista-interno.service";

describe('PareceristaExternoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PareceristaExternoService = TestBed.get(PareceristaExternoService);
    expect(service).toBeTruthy();
  });
});
