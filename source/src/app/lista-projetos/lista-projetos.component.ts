// Import of libs
import { Component, OnInit } from '@angular/core';

import { AppService } from '../app.service';
import { ListaProjetosService } from './lista-projetos.service';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { formatDate } from '@angular/common';
import { MyDatePipe } from '../shared/pipes/my-date-pipe';



@Component({
  selector: 'app-lista-projetos',
  templateUrl: './lista-projetos.component.html',
  styleUrls: ['./lista-projetos.component.scss']
})
export class ListaProjetosComponent implements OnInit {
  [x: string]: any;

  p: number = 1;
  qtdProjetos : number;
  listaProjeto : any;
  filtrado: any;

  constructor(
    public _appService: AppService,
    public _listaProjetosService: ListaProjetosService,
    public route: ActivatedRoute,
    public router: Router,
    public myDatePipe: MyDatePipe,
  ) { }

  ngOnInit() {
    this.requestProjects();

    this.router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        if (this.route.children.length === 0) {
          this.requestProjects();
        }
      }
    });
  }

  requestProjects() {
    this._listaProjetosService.listaProjeto = [];
    this._listaProjetosService.listaProjetos().subscribe(
      (data: any) => {
        if(data.length == 0){
          this._appService.showToastrWarning("Nenhum projeto encontrado.");
          return;
        }
        
        this._listaProjetosService.listaProjeto = data;
        this.filtrado = data;
    }, (error: any) => {
          this._appService.showToastrError("Não foi possível realizar a busca de projetos.");
          return;
    });
  }

  filtrarProjetos(event: any){
    let srtFiltro = this._appService.normalizarString(event.target.value.toLowerCase());

    
    const format = 'dd/MM/yyyy';
    const locale = 'en-US';

    
    this.filtrado = this._listaProjetosService.listaProjeto.filter((el) => {
      return this._appService.normalizarString(el.Ra.toLowerCase()).indexOf(srtFiltro) > -1||
             this._appService.normalizarString(el.Protocolo.toLowerCase()).indexOf(srtFiltro) > -1||
             this._appService.normalizarString(el.Aluno.toLowerCase()).indexOf(srtFiltro) > -1||
             this._appService.normalizarString(el.Email.toLowerCase()).indexOf(srtFiltro) > -1||
             this._appService.normalizarString(el.EmailOrientador.toLowerCase()).indexOf(srtFiltro) > -1||
             this._appService.normalizarString(el.Orientador.toLowerCase()).indexOf(srtFiltro) > -1||
             this._appService.normalizarString(el.Situacao.toLowerCase()).indexOf(srtFiltro) > -1||
             this._appService.normalizarString(el.Turma.toLowerCase()).indexOf(srtFiltro) > -1||
             this._appService.normalizarString(el.Campus.toLowerCase()).indexOf(srtFiltro) > -1||
             this._appService.normalizarString(el.Lates.toLowerCase()).indexOf(srtFiltro) > -1||
             formatDate(this.myDatePipe.transform(el.DataSubmissao), format, locale).indexOf(srtFiltro) > -1||
             this._appService.normalizarString(el.TipoAluno.toLowerCase()).indexOf(srtFiltro) > -1;
     });
  }

  atribuirBolsa(protocolo,status) {
    let body = {
      Protocolo : protocolo,
      BolsaAtribuida : status
    };

    this._listaProjetosService.atribuirBolsa(body).subscribe(
      (data: any) => {

        if(data){
          if(status)
            this._appService.showToastrSuccess("Aprovado(a)!, aluno(a) será notíficado(a) por e-mail.");
          else
            this._appService.showToastrWarning("Não aprovado(a)!, aluno(a) será notíficado(a) por e-mail.");
        }

        this.requestProjects();
    }, (error: any) => {
          this._appService.showToastrError("Erro ao realizar a aprovação do projeto.");
          return;
    });
  }
  
  copyMessage(val: string){
    let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }
}
