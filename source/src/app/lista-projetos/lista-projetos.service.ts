import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppService } from '../app.service';
import { baseUrl } from '../shared/shared.variables';

@Injectable()
export class ListaProjetosService {
  listaProjeto: any = [];

  constructor(private _http: HttpClient, public appService: AppService) { }

  listaProjetos() {
    let options = {
        headers: new HttpHeaders({
            'Content-Type': 'application/x-www-form-urlencoded'
        })
    }
    return this._http.get(`${baseUrl}ProjetoPesquisa/ProjetosOrientador`, options);
  }

  atribuirBolsa(obj: any) {
    return this._http.post(`${baseUrl}ProjetoPesquisa/AtribuirBolsa`, this.appService.tratarObjeto(obj), this.appService.headerOptions);
  }
}