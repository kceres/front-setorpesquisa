import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppService } from '../app.service';
import { baseUrl } from '../shared/shared.variables';

@Injectable()
export class HomeService {

  constructor(private _http: HttpClient, public appService: AppService) { 
    
  }
}