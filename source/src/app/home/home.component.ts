// Import of libs
import { Component, OnInit } from '@angular/core';

import { AppService } from '../app.service';
import { HomeService } from './home.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  gridActive: boolean = true;
  
  constructor(
    public _homeService: HomeService) { 

    }

  ngOnInit() {}
}
