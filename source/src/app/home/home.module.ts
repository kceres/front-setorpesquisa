

// Imports of libs
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Import of modules, components and services
import { HomeComponent } from './home.component';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [ HomeComponent ],
  declarations: [ HomeComponent ]
})
export class HomeModule { }
