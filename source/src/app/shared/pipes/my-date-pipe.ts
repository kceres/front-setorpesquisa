import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'myDate'
})

export class MyDatePipe implements PipeTransform {
  transform(value: string): any {
    
    if(value == null)
    return new Date(value);
    
    if(value.length > 10) {      
      let rawValue = new Date(parseInt(value.substr(6)));
      let data = new Date(rawValue.valueOf() + rawValue.getTimezoneOffset() * 20000);
      return data;
    } else {
      return new Date(value);
    }
    
  }
}