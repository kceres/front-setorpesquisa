export const baseUrl = defineUrl();

function defineUrl() {
    if(window.location.hostname == 'ict.unip.br') { // PRODUÇÃO
        return '//ict.unip.br/api/';
    }
    //  return 'http://localhost:50209/'; // DEV
    //  return 'http://200.136.94.100/'; // HOMOLOGAÇÃO OU LOCALHOST
}