// Import of libs
import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

import { AppService } from '../app.service';
import { OrientadorDadosService } from './orientador-dados.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ConnectionBackend } from '@angular/http';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import * as $ from 'jquery';
import { identifierModuleUrl } from '@angular/compiler';
import { baseUrl } from '../shared/shared.variables';


@Component({
  selector: 'app-orientador-dados',
  templateUrl: './orientador-dados.component.html',
  styleUrls: ['./orientador-dados.component.scss']
})
export class OrientadorDadosComponent implements OnInit {

  qtdProjetos: number;
  professor: any;
  titulacaoDr: boolean = false;
  titulacaoMestre: boolean = false;
  titulacao : any;
  lates: any= '';
  listaGrupos: any;
  listaAreaAtuacao: any;
  arrayAreaAtuacao: any = [];
  idGrupoAreaAtuacao: number;

  formOrientador: FormGroup;
  formOrientadorGrupo: FormGroup;
  veriryLoader: boolean;

  constructor(
    public _appService: AppService,
    public _orientadorDadosService: OrientadorDadosService,
    public route: ActivatedRoute,
    private _location: Location,
    public router: Router
  ) {
    this.formOrientador = new FormGroup({
      numeroFuncional: new FormControl(),
      titulacao: new FormControl(),
      lates: new FormControl(this.lates, Validators.compose([Validators.required]))
    });

    this.formOrientadorGrupo = new FormGroup({
      numeroFuncional: new FormControl(),
      titulacaoDoutor: new FormControl(),
      titulacaoMestre: new FormControl(),
      lates: new FormControl(this.lates, Validators.compose([Validators.required]))
    });
  }

  ngOnInit() {
    this._orientadorDadosService.Retorna(this._appService.numeroFuncional).subscribe((data: any) => {
      this.professor = data;
      this.lates = this.professor.Lates;
      this.populateTitulacao();
      this.listarGrupos();
    }, (error: any) => {
      this._appService.showToastrError(`Ocorreu algum erro inesperado`);
      console.log(error);
    })
  }

  doSubmit() {
    // // let body = {
    // //   NumeroFuncional : this._appService.numeroFuncional,
    // //   Titulacao: this.retornaTitulacao(),
    // //   Lates: this.lates
    // // }

    let body = `&numeroFuncional=${this._appService.numeroFuncional}&titulacao=${this.retornaTitulacao()}&lates=${this.lates}`;
    this.veriryLoader = true;

    if(this.formOrientador.valid) {
      this._orientadorDadosService.Alterar(body)
        .subscribe(
          res => {
            this.veriryLoader = !this.veriryLoader;
            this._appService.showToastrSuccess(`Cadastro salvo com sucesso`);
          },
          err => {
            console.log(err);          
            this._appService.showToastrError(`Ocorreu algum erro inesperado`);
          }
        );
    } else {
      $('input.ng-invalid').eq(0).focus();
      this._appService.showToastrWarning('Ainda existem campos inválidos. Por favor, preencha-os');
      this._appService.verificaValidacoesForm(this.formOrientador);
    }
  }
  doSubmitGroup() {
    let body = this.formOrientadorGrupo.value;

    this.veriryLoader = true;
    this._orientadorDadosService.SalvarGrupo(body)
      .subscribe(
        res => {
          this.veriryLoader = !this.veriryLoader;
          this.router.navigate(["/orientador-dados"]);
        },
        err => {
          console.log(err)
          this.router.navigate(["/orientador-dados"]);
        }
      );
  }

  listarGrupos() {
    this._orientadorDadosService.ListarGrupo(this._appService.numeroFuncional).subscribe((data: any) => {
      this.listaGrupos = data;
      
    }, (error: any) => {
      console.log(error);
    })
  }

  loadAreaAtuacao(idGrupo: number) {
    this.listaAreaAtuacao = [];
    this.arrayAreaAtuacao = [];
    this.idGrupoAreaAtuacao = idGrupo;
    this._orientadorDadosService.AreaAtuacaoOrientador(this._appService.numeroFuncional, idGrupo).subscribe((data: any) => {
      
      this.listaAreaAtuacao = data;     

      for(let i = 0; i < this.listaAreaAtuacao.length; i++) {
        if(this.listaAreaAtuacao[i].Pertence) {
          this.arrayAreaAtuacao.push(this.listaAreaAtuacao[i].IdAreaAtuacao);
        }
      }

      
    }, (error: any) => {
      console.log(error);
    })
  }

  postAreaAtuacaoOrientador() {
    let obj = {
      IdAreaAtuacao: this.arrayAreaAtuacao,      
      NumeroFuncional: this._appService.numeroFuncional,
      GrupoId: this.idGrupoAreaAtuacao
    }

    $.ajax({
      url: `${baseUrl}AreaAtuacao/InsereOrientador`,
      method:"POST",
      data: obj,
      success: (data) => {
        this._appService.showToastrSuccess(`Dados salvos com sucesso`);
      },
      error: (error) => {
        console.log(error);
        this._appService.showToastrError(`Ocorreu algum erro inesperado`);
      }
    });
  }

  populateArrayAreaAtuacao(idAreaAtuacao) {
    if(this.arrayAreaAtuacao.includes(idAreaAtuacao)) {
      let indexOfItem = this.arrayAreaAtuacao.indexOf(idAreaAtuacao);
      this.arrayAreaAtuacao.splice(indexOfItem, 1);
    } else {
      this.arrayAreaAtuacao.push(idAreaAtuacao);
    }
  }

  retornaTitulacao() {

    if (this.titulacaoDr && this.titulacaoMestre) {
      return "A"
    } else if (this.titulacaoDr) {
      return "D"
    } else if ( this.titulacaoMestre ) {
      return "M"
    }
  }

  populateTitulacao() {
    if(this.professor.Titulacao == 'M') {
      this.titulacaoMestre = true;
    } else if(this.professor.Titulacao == 'D') {
      this.titulacaoDr = true;
    } else {
      this.titulacaoMestre = true;
      this.titulacaoDr = true;
    }
  }

// //   tratarObjetoForm(obj : any, idPadraoAcesso?) {
// //     var newObj = '';
// //     let array = Object.getOwnPropertyNames(obj);
// //     $.each(array, (index, value) => {
// //         var aux =  obj[index].value.toString().split(',');
// //         if(aux.length > 0) {
// //             for(var i = 0; i < aux.length; i++)
// //             {
// //                 newObj += value + '=' + aux[i] + "&"
// //             }
// //         } else {
// //             newObj += value + '=' + obj[value].value + "&"
// //         }

// //     })
// //     return newObj;
// // }
}
