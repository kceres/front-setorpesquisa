import { TestBed, inject } from '@angular/core/testing';

import { OrientadorDadosService } from './orientador-dados.service';

describe('OrientadorDadosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OrientadorDadosService]
    });
  });

  it('should be created', inject([OrientadorDadosService], (service: OrientadorDadosService) => {
    expect(service).toBeTruthy();
  }));
});
