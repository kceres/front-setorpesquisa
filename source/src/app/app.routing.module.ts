// Imports of libs
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Imports of components, modules and service
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { ListaProjetosComponent } from './lista-projetos/lista-projetos.component';
import { OrientadorDadosComponent } from './orientador-dados/orientador-dados.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { VisualizacaoProjetoComponent } from './lista-projetos/visualizacao-projeto/visualizacao-projeto.component';
import { PareceristaInternoComponent } from './parecerista-interno/parecerista-interno.component';
import { PareceristaExternoComponent } from './parecerista-externo/parecerista-externo.component';
import { GrupoPesquisaComponent } from './grupo-pesquisa/grupo-pesquisa.component';
import { RankingComponent } from './ranking/ranking.component';
import { RankingExternoComponent } from './ranking-externo/ranking-externo.component';

const appRoutes : Routes = [
  { path: 'lista-projetos', component: ListaProjetosComponent, children: [{
    path: ':id', component: VisualizacaoProjetoComponent
  }]},
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },  
  { path: 'orientador-dados', component: OrientadorDadosComponent },  
  { path: 'parecerista-interno', component: PareceristaInternoComponent }, 
  { path: 'parecerista-externo', component: PareceristaExternoComponent },  
  { path: 'grupo-pesquisa', component: GrupoPesquisaComponent },  
  { path: 'ranking', component: RankingComponent }, 
  { path: 'ranking-externo', component: RankingExternoComponent }, 
  { path: '', redirectTo: "login", pathMatch: "full" },
  { path: '**', component: NotFoundComponent }

]

@NgModule({
  imports: [ RouterModule.forRoot(appRoutes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule { }